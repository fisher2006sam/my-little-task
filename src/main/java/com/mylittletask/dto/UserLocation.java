package com.mylittletask.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.NonNull;

/**
 * This is an object which is a part of returning response user location data
 *
 * @author Erchenko Igor
 */
public record UserLocation(
        @NonNull
        @ApiModelProperty(name = "latitude", value = "User location latitude", example = "57.25742342295784")
        Double latitude,
        @NonNull
        @ApiModelProperty(name = "longitude", value = "User location latitude", example = "23.540583401747602")
        Double longitude
) {
}
