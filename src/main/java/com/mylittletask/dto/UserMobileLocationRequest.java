package com.mylittletask.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.NonNull;

/**
 * This is an object of receiving request userdata
 *
 * @author Erchenko Igor
 */
public record UserMobileLocationRequest(
        @NonNull
        @ApiModelProperty(name = "userId", value = "User ID", example = "2e3b11b0-07a4-4873-8de5-d2ae2eab26b2") String userId,
        @NonNull
        @ApiModelProperty(name = "createdOn", value = "Date and time of creation", example = "2022-02-08T11:49:22.524") String createdOn,
        UserLocation location
) {
}
