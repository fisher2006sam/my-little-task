package com.mylittletask.dto.responses;

import com.mylittletask.dto.LocationData;
import io.swagger.annotations.ApiModelProperty;

import java.util.List;

/**
 * This is an object that uses as response for user location data
 *
 * @author Erchenko Igor
 */
public record UserLocationsResponse(
        @ApiModelProperty(name = "userId", value = "User ID", example = "2e3b11b0-07a4-4873-8de5-d2ae2eab26b2") String userId,
        List<LocationData> locationData) {
}
