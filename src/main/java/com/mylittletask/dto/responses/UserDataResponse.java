package com.mylittletask.dto.responses;

import com.mylittletask.dto.UserLocation;
import io.swagger.annotations.ApiModelProperty;

/**
 * This is an object that uses as response user data
 *
 * @author Erchenko Igor
 */
public record UserDataResponse(
        @ApiModelProperty(name = "userId", value = "User ID", example = "2e3b11b0-07a4-4873-8de5-d2ae2eab26b2") String userId,
        @ApiModelProperty(name = "email", value = "User email", example = "alex.schmid@gmail.com") String email,
        @ApiModelProperty(name = "firstName", value = "Users firstName", example = "Alex")String firstName,
        @ApiModelProperty(name = "secondName", value = "Users secondName", example = "Schmid")String secondName,
        UserLocation location
) {
}
