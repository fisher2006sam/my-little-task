package com.mylittletask.dto;

import io.swagger.annotations.ApiModelProperty;

/**
 * This is an object which is a part of returning response user location data
 *
 * @author Erchenko Igor
 */
public record LocationData(
        @ApiModelProperty(name = "createdOn", value = "Date and time of creation", example = "2022-02-08T11:49:22.524")  String createdOn,
        UserLocation locations) {
}
