package com.mylittletask.controller;

import com.mylittletask.dto.UserLocation;
import com.mylittletask.dto.responses.UserDataResponse;
import com.mylittletask.dto.responses.UserLocationsResponse;
import com.mylittletask.service.UserDataService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestBody;
import reactor.core.publisher.Mono;

import java.util.Map;

/**
 * This is a user controller. Base functionality is:
 * - Deliver/Save/Update data about user
 * - Deliver last user location data
 * - Deliver list of saved users locations
 *
 * @author Erchenko Igor
 */
@Slf4j
@RestController
@RequestMapping("/user")
@ApiOperation(value = "/user", tags = "This controller for back requests (adding/getting/updating data about user and users location)")
public class UserController {

    private final UserDataService dataService;

    public UserController(UserDataService dataService) {
        this.dataService = dataService;
    }

    @ApiOperation(
            value = "Adding or updating user data to the database",
            notes = "This method allow save or update user data in database. You should use construction like shown below for changing user data",
            response = Mono.class,
            httpMethod = "POST"
    )
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "SUCCESS", response = String.class),
            @ApiResponse(code = 404, message = "NOT FOUND", response = String.class),
    })
    @PostMapping("/update")
    public Mono<String> saveOrUpdateUserData(
            @RequestBody
            @ApiParam(
                    name = "Save/Update user data in db",
                    value = "Incoming users data",
                    example = """
                            {
                                "userId": "2e3b11b0-07a4-4873-8de5-d2ae2eab26b2",
                                "email": "alex.schmid@gmail.com",
                                "firstName": "Alex",
                                "secondName": "Schmid"
                            }
                            """,
                    required = true
            )
            Map<String, String> userDataDto
    ) {
        log.info(String.format("Starting adding/updating user %s", userDataDto.get("userId")));
        return dataService.saveOrUpdateUserService(userDataDto)
                .log()
                .map(e -> String.format("User with id %s successfully saved", e.getUserId()));
    }

    @ApiOperation(
            value = "Get last users location",
            notes = "Method allow to get latest users location with users some data like email, firstname and secondname",
            response = Mono.class,
            httpMethod = "GET"
    )
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "SUCCESS", response = UserDataResponse.class),
            @ApiResponse(code = 404, message = "NOT FOUND", response = UserDataResponse.class),
    })
    @GetMapping("/location")
    public Mono<UserDataResponse> getUserLatestLocation(
            @RequestParam
            @ApiParam(
                    name = "userId",
                    value = "Id user example",
                    example = "2e3b11b0-07a4-4873-8de5-d2ae2eab26b2"
            )
            String userId
    ) {
        log.info(userId);
        return dataService.getZippeddata(userId)
                .log()
                .map(data -> {
                    UserLocation location = new UserLocation(data.getT2().getLatitude(), data.getT2().getLongitude());
                    return new UserDataResponse(
                            data.getT1().getUserId(),
                            data.getT1().getEmail(),
                            data.getT1().getFirstName(),
                            data.getT1().getSecondName(),
                            location
                    );
                });
    }

    @ApiOperation(value = "Get list of users location", notes = "Method allow to get list users locations", response = Mono.class,
            httpMethod = "GET")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "SUCCESS", response = UserLocationsResponse.class),
            @ApiResponse(code = 404, message = "NOT FOUND", response = UserLocationsResponse.class),
    })
    @GetMapping("/locations")
    public Mono<UserLocationsResponse> getAllUserLatestLocations(
            @RequestParam
            @ApiParam(
                    name = "userId",
                    value = "Id user example",
                    example = "2e3b11b0-07a4-4873-8de5-d2ae2eab26b2"
            )
            String userId
    ) {
        log.info(userId);
        return dataService.getLocationData(userId)
                .map(tuple -> new UserLocationsResponse(userId, tuple.getT2().stream().toList()));
    }
}
