package com.mylittletask.controller;

import com.mylittletask.document.UserLocationDocument;
import com.mylittletask.dto.UserMobileLocationRequest;
import com.mylittletask.service.UserDataService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

/**
 * This is a mobile controller. Base functionality is:
 * - Receiving new user locations
 * - Adding new location to database
 *
 * @author Erchenko Igor
 */
@Slf4j
@RestController
@RequestMapping("/mobile")
@ApiOperation(value = "/mobile", tags = "This controller for mobile requests (adding new user geoposition data)")
public class MobileController {

    private final UserDataService dataService;

    public MobileController(UserDataService dataService) {
        this.dataService = dataService;
    }

    @ApiOperation(
            value = "Adding user location to the database",
            notes = "Method add new location to database by received UserMobileLocationRequest object",
            response = Mono.class,
            httpMethod = "POST"
    )
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "SUCCESS", response = ResponseEntity.class),
            @ApiResponse(code = 404, message = "NOT FOUND", response = ResponseEntity.class),
    })
    @PostMapping("/location")
    public Mono<ResponseEntity<String>> updateUserLocation(@RequestBody UserMobileLocationRequest userLocationRequest) {
        log.info(userLocationRequest.toString());
        UserLocationDocument userLocation = UserLocationDocument.builder()
                .userId(userLocationRequest.userId())
                .createdOn(userLocationRequest.createdOn())
                .latitude(userLocationRequest.location().latitude())
                .longitude(userLocationRequest.location().longitude())
                .build();
        return dataService.saveLocation(userLocation)
                .map(e -> ResponseEntity.ok().body("Location of user " + userLocationRequest.userId() + " saved!"));
    }
}
