package com.mylittletask.service;

import com.mylittletask.document.UserDataDocument;
import com.mylittletask.document.UserLocationDocument;
import com.mylittletask.dto.LocationData;
import com.mylittletask.dto.UserLocation;
import com.mylittletask.repository.UserDataRepository;
import com.mylittletask.repository.UserLocationsRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;
import reactor.core.publisher.Mono;
import reactor.util.function.Tuple2;

import java.util.List;
import java.util.Map;

/**
 * This is a data delivery service. Base functionality is:
 * - Communication between repositories
 * - Delivering and transform data
 *
 * @author Erchenko Igor
 */
@Service
public class UserDataService {

    private final UserDataRepository dataRepository;
    private final UserLocationsRepository locationsRepository;

    public UserDataService(UserDataRepository dataRepository, UserLocationsRepository locationsRepository) {
        this.dataRepository = dataRepository;
        this.locationsRepository = locationsRepository;
    }

    public Mono<Tuple2<UserDataDocument, UserLocationDocument>> getZippeddata(String userId) {
        Mono<UserDataDocument> userData = dataRepository
                .findUserDataDocumentByUserId(userId)
                .log();
        Mono<UserLocationDocument> userLocation = locationsRepository.findAllByUserId(userId)
                .sort((obj1, obj2) -> obj2.getCreatedOn().compareToIgnoreCase(obj1.getCreatedOn()))
                .log()
                .next();

        return Mono.zip(userData, userLocation);
    }

    public Mono<Tuple2<String, List<LocationData>>> getLocationData(String userId) {
        Mono<List<LocationData>> locationData = locationsRepository.findAllByUserId(userId)
                .map(data -> new LocationData(
                        data.getCreatedOn(),
                        new UserLocation(data.getLatitude(), data.getLongitude()))
                )
                .log()
                .collectList();

        return Mono.zip(Mono.just(userId), locationData);
    }

    @Transactional(isolation = Isolation.SERIALIZABLE)
    public Mono<UserDataDocument> saveOrUpdateUserService(Map<String, String> userDataDto) {
        UserDataDocument userDataDocument = UserDataDocument.builder()
                .userId(userDataDto.get("userId"))
                .email(userDataDto.get("email"))
                .firstName(userDataDto.get("firstName"))
                .secondName(userDataDto.get("secondName"))
                .build();
        return Mono.from(dataRepository.save(userDataDocument));
    }

    @Transactional(isolation = Isolation.SERIALIZABLE)
    public Mono<UserLocationDocument> saveLocation(UserLocationDocument userLocation) {
        return locationsRepository.save(userLocation);
    }
}
