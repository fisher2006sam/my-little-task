package com.mylittletask.document;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.ToString;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * This is an object for mongodb communicating that deliver information about user location
 *
 * @author Erchenko Igor
 */
@Document
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
public class UserLocationDocument {
    private String userId;
    private Double latitude;
    private Double longitude;
    private String createdOn;
}
