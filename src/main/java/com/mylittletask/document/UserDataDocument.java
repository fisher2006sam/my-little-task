package com.mylittletask.document;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.ToString;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * This is an object for mongodb communicating that keep information about user
 *
 * @author Erchenko Igor
 */
@Document
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
public class UserDataDocument {
    @Id
    private String userId;
    private String email;
    private String firstName;
    private String secondName;
}
