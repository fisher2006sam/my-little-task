package com.mylittletask.repository;

import com.mylittletask.document.UserLocationDocument;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;

@Repository
public interface UserLocationsRepository extends ReactiveMongoRepository<UserLocationDocument, Long> {
    Flux<UserLocationDocument> findAllByUserId(String userId);
}
