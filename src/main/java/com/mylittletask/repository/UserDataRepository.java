package com.mylittletask.repository;

import com.mylittletask.document.UserDataDocument;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Mono;

@Repository
public interface UserDataRepository extends ReactiveMongoRepository<UserDataDocument, Long> {
    Mono<UserDataDocument> findUserDataDocumentByUserId(String userId);
}
