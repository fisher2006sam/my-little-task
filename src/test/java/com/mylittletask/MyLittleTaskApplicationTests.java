package com.mylittletask;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class MyLittleTaskApplicationTests {

    @Test
    void contextLoads() {
    }

}
