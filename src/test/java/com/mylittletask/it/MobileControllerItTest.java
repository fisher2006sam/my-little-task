package com.mylittletask.it;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.mylittletask.config.DefaultItConfig;
import com.mylittletask.controller.MobileController;
import com.mylittletask.document.UserDataDocument;
import com.mylittletask.dto.UserMobileLocationRequest;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.autoconfigure.data.mongo.AutoConfigureDataMongo;
import org.springframework.boot.test.autoconfigure.web.reactive.WebFluxTest;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * This is a test for checking full functionality mobile controller:
 * - Receiving data in controller
 * - Saving/getting it into/from emulated database
 *
 * @author Erchenko Igor
 */
@WebFluxTest(MobileController.class)
@AutoConfigureDataMongo
class MobileControllerItTest extends DefaultItConfig {

    @Test
    void updateUserLocation() throws JsonProcessingException {
        // For adding/updating user location data we should have data about user in database
        // So we should prepare it to make sure thats all work fine
        UserDataDocument userDataDocument = UserDataDocument.builder()
                .userId("2e3b11b0-07a4-4873-8de5-d2ae2eab26b2")
                .email("alex.schmid@gmail.com")
                .firstName("Alex")
                .secondName("Schmid")
                .build();

        // Adding userdata to database and appending searching added document for check
        Flux<UserDataDocument> initial = Mono.from(userDataRepository.deleteAll()
                        .thenMany(userDataRepository.save(userDataDocument)))
                .thenMany(userDataRepository.findUserDataDocumentByUserId("2e3b11b0-07a4-4873-8de5-d2ae2eab26b2"));

        // Checking that userdata saved into database
        StepVerifier
                .create(initial)
                .expectSubscription()
                .consumeNextWith(data ->
                        assertEquals(data.getUserId(), "2e3b11b0-07a4-4873-8de5-d2ae2eab26b2")
                )
                .verifyComplete();

        // Request payload with users location
        UserMobileLocationRequest locationRequest = mapper.readValue(mobileLocation, UserMobileLocationRequest.class);
        webClient
                .post().uri("/mobile/location")
                .bodyValue(locationRequest)
                .exchange()
                .expectStatus()
                .isOk();

        // Checking that last request well done and updated data added to database
        StepVerifier
                .create(Mono.from(userLocationsRepository.findAllByUserId(locationRequest.userId())))
                .expectSubscription()
                .consumeNextWith(data -> {
                    assertEquals(data.getLatitude(), 57.25742342295784);
                    assertEquals(data.getLongitude(), 23.540583401747602);
                })
                .verifyComplete();
    }
}