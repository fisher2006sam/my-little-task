package com.mylittletask.it;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.mylittletask.config.DefaultItConfig;
import com.mylittletask.controller.UserController;
import com.mylittletask.document.UserDataDocument;
import com.mylittletask.document.UserLocationDocument;
import com.mylittletask.dto.LocationData;
import com.mylittletask.dto.UserLocation;
import com.mylittletask.dto.UserMobileLocationRequest;
import com.mylittletask.dto.responses.UserDataResponse;
import com.mylittletask.dto.responses.UserLocationsResponse;
import org.junit.jupiter.api.Test;
import org.reactivestreams.Publisher;
import org.springframework.boot.test.autoconfigure.web.reactive.WebFluxTest;
import reactor.test.StepVerifier;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

/**
 * This is a test for checking full functionality back controller:
 * - Receiving data in controller
 * - Saving/getting it into/from emulated database
 * - Getting users location(s) data that keeped into database
 *
 * @author Erchenko Igor
 */
@WebFluxTest(value = UserController.class)
class UserControllerItTest extends DefaultItConfig {

    @Test
    void saveOrUpdateUserData() throws JsonProcessingException {
        UserDataDocument dataDocument = mapper.readValue(userData, UserDataDocument.class);

        // Sending request to controller for adding or updating userdata
        webClient.post()
                .uri("/user/update")
                .bodyValue(dataDocument)
                .exchange()
                .expectBody(String.class)
                .isEqualTo("User with id 2e3b11b0-07a4-4873-8de5-d2ae2eab26b2 successfully saved")
        ;

        // Checking that userdata saved into database after request
        StepVerifier
                .create(userDataRepository.findUserDataDocumentByUserId(dataDocument.getUserId()))
                .expectSubscription()
                .consumeNextWith(data -> {
                    assertEquals(data.getUserId(), "2e3b11b0-07a4-4873-8de5-d2ae2eab26b2");
                    assertEquals(data.getFirstName(), "Alex");
                    assertNotEquals(data.getFirstName(), "Alen");
                })
                .verifyComplete();
    }

    @Test
    void getUserLatestLocation() throws JsonProcessingException {
        // Adding data to database for getting it by request
        UserDataDocument dataDocument = mapper.readValue(userData, UserDataDocument.class);
        UserMobileLocationRequest mobileLocationRequest = mapper.readValue(mobileLocation, UserMobileLocationRequest.class);
        UserLocationDocument locationDocument = new UserLocationDocument(
                mobileLocationRequest.userId(), mobileLocationRequest.location().latitude(),
                mobileLocationRequest.location().longitude(), mobileLocationRequest.createdOn()
        );

        // Checking that data saved in database correctly
        Publisher<UserLocationDocument> publisher = userDataRepository.deleteAll()
                .thenMany(userLocationsRepository.deleteAll())
                .thenMany(userDataRepository.save(dataDocument))
                .thenMany(userLocationsRepository.save(locationDocument))
                .thenMany(userLocationsRepository.findAllByUserId(defaultUserId));
        StepVerifier.create(publisher)
                .consumeNextWith(data -> assertEquals(data, locationDocument))
                .verifyComplete();

        // Creating what we are expecting from response
        UserDataResponse expectedUserDataResponse = new UserDataResponse(
                dataDocument.getUserId(),
                dataDocument.getEmail(),
                dataDocument.getFirstName(),
                dataDocument.getSecondName(),
                new UserLocation(locationDocument.getLatitude(), locationDocument.getLongitude())
        );


        // Sending request to controller for getting userdata and checking it
        webClient
                .get().uri(uriBuilder -> uriBuilder
                        .scheme("http")
                        .host("localhost")
                        .port("9000")
                        .path("/user/location")
                        .queryParam("userId", defaultUserId)
                        .build()
                )
                .exchange()
                .expectStatus()
                .isOk()
                .expectBody(UserDataResponse.class)
                .isEqualTo(expectedUserDataResponse);
    }

    @Test
    void getAllUserLatestLocations() throws JsonProcessingException {
        // Adding data to database for getting it by request
        UserDataDocument dataDocument = mapper.readValue(userData, UserDataDocument.class);
        UserMobileLocationRequest mobileLocationRequest = mapper.readValue(mobileLocation, UserMobileLocationRequest.class);
        UserLocationDocument locationDocument = new UserLocationDocument(
                mobileLocationRequest.userId(), mobileLocationRequest.location().latitude(),
                mobileLocationRequest.location().longitude(), mobileLocationRequest.createdOn()
        );

        // Checking that data saved in database correctly
        Publisher<UserLocationDocument> publisher = userDataRepository.deleteAll()
                .thenMany(userLocationsRepository.deleteAll())
                .thenMany(userDataRepository.save(dataDocument))
                .thenMany(userLocationsRepository.save(locationDocument))
                .thenMany(userLocationsRepository.findAllByUserId(defaultUserId));
        StepVerifier.create(publisher)
                .consumeNextWith(data -> assertEquals(data, locationDocument))
                .verifyComplete();

        // Creating expected params
        List<LocationData> expectedLocation = List.of(
                new LocationData(locationDocument.getCreatedOn(), new UserLocation(locationDocument.getLatitude(), locationDocument.getLongitude()))
        );
        UserLocationsResponse expectedUserDataResponse = new UserLocationsResponse(defaultUserId, expectedLocation);

        // Sending request to controller for getting userdata and checking it
        webClient
                .get().uri(uriBuilder -> uriBuilder
                        .scheme("http")
                        .host("localhost")
                        .port("9000")
                        .path("/user/locations")
                        .queryParam("userId", defaultUserId)
                        .build()
                )
                .exchange()
                .expectStatus()
                .isOk()
                .expectBody(UserLocationsResponse.class)
                .isEqualTo(expectedUserDataResponse);
    }

}