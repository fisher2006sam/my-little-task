package com.mylittletask.config;

import com.mylittletask.repository.UserDataRepository;
import com.mylittletask.repository.UserLocationsRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mylittletask.service.UserDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.reactive.server.WebTestClient;

/**
 * This is a default components for unit testing:
 *
 * @author Erchenko Igor
 */
public class SetupConfig {

    @MockBean
    public UserLocationsRepository userLocationsRepository;

    @MockBean
    public UserDataRepository userDataRepository;

    @MockBean
    public UserDataService dataService;

    @Autowired
    public WebTestClient webClient;

    @Autowired
    public ObjectMapper mapper;
}
