package com.mylittletask.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mylittletask.repository.UserDataRepository;
import com.mylittletask.repository.UserLocationsRepository;
import com.mylittletask.service.UserDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.mongo.AutoConfigureDataMongo;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.test.web.reactive.server.WebTestClient;

/**
 * This is a default components for integration testing:
 *
 * @author Erchenko Igor
 */
@AutoConfigureDataMongo
@ComponentScan(basePackages = "com.mylittletask")
public class DefaultItConfig {

    @Autowired
    public WebTestClient webClient;

    @Autowired
    public UserDataRepository userDataRepository;

    @Autowired
    public UserLocationsRepository userLocationsRepository;

    @Autowired
    public UserDataService dataService;

    @Autowired
    public ObjectMapper mapper;

    public final String defaultUserId = "2e3b11b0-07a4-4873-8de5-d2ae2eab26b2";

    public final String mobileLocation = """
                {
                    "userId": "2e3b11b0-07a4-4873-8de5-d2ae2eab26b2",
                    "createdOn": "2022-02-08T11:49:22.524",
                    "location": {
                        "latitude": 57.25742342295784,
                        "longitude": 23.540583401747602
                    }
                }
                """;

    public final String userData = """
                {
                    "userId": "2e3b11b0-07a4-4873-8de5-d2ae2eab26b2",
                    "email": "alex.schmid@gmail.com",
                    "firstName": "Alex",
                    "secondName": "Schmid"
                }
                """;
}
