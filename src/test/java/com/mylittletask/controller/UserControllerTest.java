package com.mylittletask.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.mylittletask.config.SetupConfig;
import com.mylittletask.document.UserDataDocument;
import com.mylittletask.document.UserLocationDocument;
import com.mylittletask.dto.LocationData;
import com.mylittletask.dto.UserLocation;
import com.mylittletask.dto.responses.UserDataResponse;
import com.mylittletask.dto.responses.UserLocationsResponse;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.boot.test.autoconfigure.web.reactive.WebFluxTest;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;

/**
 * This is a test for checking particular functionality back controller:
 * - Receiving data in controller
 * - Emulating saving/getting data into/from emulated database
 * - Emulating getting users location(s) data that keeped into database
 *
 * @author Erchenko Igor
 */
@WebFluxTest(UserController.class)
class UserControllerTest extends SetupConfig {

    @Test
    void saveOrUpdateUserDataSuccess() throws JsonProcessingException {
        String request = """
                {
                    "userId": "2e3b11b0-07a4-4873-8de5-d2ae2eab26b2",
                    "email": "alex.schmid@gmail.com",
                    "firstName": "Alex",
                    "secondName": "Schmid"
                }
                """;
        TypeReference<HashMap<String, String>> typeRef = new TypeReference<>() {
        };
        Map<String, String> userDataRequest = mapper.readValue(request, typeRef);

        UserDataDocument userDataDocument = UserDataDocument.builder()
                .userId(userDataRequest.get("userId"))
                .email(userDataRequest.get("email"))
                .firstName(userDataRequest.get("firstName"))
                .secondName(userDataRequest.get("secondName"))
                .build();
        Mockito.when(userDataRepository.save(any())).thenReturn(Mono.just(userDataDocument));
        Mockito.when(dataService.saveOrUpdateUserService(any())).thenReturn(Mono.just(userDataDocument));

        webClient
                .post().uri("/user/update")
                .bodyValue(userDataRequest)
                .exchange()
                .expectStatus()
                .isOk()
                .expectBody(String.class)
                .consumeWith(data -> {
                    assertNotNull(data);
                    assertEquals(data.getResponseBody(), String.format("User with id %s successfully saved", userDataDocument.getUserId()));
                })
        ;
    }

    @Test
    void getUserLatestLocationSuccess() {
        initialize();
        webClient
                .get().uri(uriBuilder -> uriBuilder
                        .scheme("http")
                        .host("localhost")
                        .port("9000")
                        .path("/user/location")
                        .queryParam("userId", "2e3b11b0-07a4-4873-8de5-d2ae2eab26b2")
                        .build()
                )
                .exchange()
                .expectStatus()
                .isOk()
                .expectBody(UserDataResponse.class);
    }

    @Test
    void getUserLatestLocationNoParams() {
        initialize();
        webClient
                .get().uri("/user/location")
                .exchange()
                .expectStatus()
                .isBadRequest()
                .expectBody(UserDataResponse.class);
    }

    @Test
    void getAllUserLatestLocationsSuccess() {
        initialize();
        webClient
                .get().uri(uriBuilder -> uriBuilder
                        .scheme("http")
                        .host("localhost")
                        .port("9000")
                        .path("/user/locations")
                        .queryParam("userId", "2e3b11b0-07a4-4873-8de5-d2ae2eab26b2")
                        .build()
                )
                .exchange()
                .expectStatus()
                .isOk()
                .expectBody(UserLocationsResponse.class)
                .consumeWith(data -> {
                    assertNotNull(data);
                    assertEquals(Objects.requireNonNull(data.getResponseBody()).userId(), "2e3b11b0-07a4-4873-8de5-d2ae2eab26b2");
                    assertEquals(data.getResponseBody().locationData().size(), 1);
                    Assertions.assertEquals(data.getResponseBody().locationData().get(0).createdOn(), "2022-02-08T11:44:00.524");
                    Assertions.assertEquals(data.getResponseBody().locationData().get(0).locations().latitude(), 52.25742342295784);
                    Assertions.assertEquals(data.getResponseBody().locationData().get(0).locations().longitude(), 10.540583401747602);
                });
    }

    private void initialize() {
        UserDataDocument userDataDocument = UserDataDocument.builder()
                .userId("2e3b11b0-07a4-4873-8de5-d2ae2eab26b2")
                .email("alex.schmid@gmail.com")
                .firstName("Alex")
                .secondName("Schmid")
                .build();

        UserLocationDocument locationDocument = UserLocationDocument.builder()
                .userId("2e3b11b0-07a4-4873-8de5-d2ae2eab26b2")
                .createdOn("2022-02-08T11:44:00.524")
                .latitude(52.25742342295784)
                .longitude(10.540583401747602)
                .build();

        UserLocation userLocation = new UserLocation(locationDocument.getLatitude(), locationDocument.getLongitude());
        LocationData locationData = new LocationData(locationDocument.getCreatedOn(), userLocation);

        Mockito.when(userDataRepository.findUserDataDocumentByUserId(anyString()))
                .thenReturn(Mono.just(userDataDocument));
        Mockito.when(userLocationsRepository.findAllByUserId(anyString()))
                .thenReturn(Flux.just(locationDocument));
        Mockito.when(dataService.getZippeddata(any()))
                .thenReturn(Mono.zip(Mono.just(userDataDocument), Mono.just(locationDocument)));
        Mockito.when(dataService.getLocationData(anyString()))
                .thenReturn(Mono.zip(Mono.just(userDataDocument.getUserId()), Mono.just(List.of(locationData))));
    }
}