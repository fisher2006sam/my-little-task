package com.mylittletask.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.mylittletask.config.SetupConfig;
import com.mylittletask.dto.UserMobileLocationRequest;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.boot.test.autoconfigure.web.reactive.WebFluxTest;
import org.springframework.http.ResponseEntity;
import reactor.core.publisher.Mono;

import static org.mockito.ArgumentMatchers.any;

/**
 * This is a test for checking particular functionality mobile controller:
 * - Receiving data in controller
 * - Emulating saving/getting data into/from emulated database
 *
 * @author Erchenko Igor
 */
@WebFluxTest(MobileController.class)
class MobileControllerTest extends SetupConfig {

    @Test
    void updateUserLocationSuccess() throws JsonProcessingException {
        String request = """
                {
                  "createdOn": "2022-02-08T11:49:22.524",
                  "location": {
                    "latitude": 57.25742342295784,
                    "longitude": 23.5405834017476
                  },
                  "userId": "2e3b11b0-07a4-4873-8de5-d2ae2eab26b2"
                }
                """;
        UserMobileLocationRequest locationRequest = mapper.readValue(request, UserMobileLocationRequest.class);

        Mockito.when(userLocationsRepository.save(any())).thenReturn(Mono.empty());
        Mockito.when(dataService.saveLocation(any())).thenReturn(Mono.empty());

        webClient
                .post().uri("/mobile/location")
                .bodyValue(locationRequest)
                .exchange()
                .expectStatus()
                .isOk()
                .expectBody(ResponseEntity.class);
    }
}