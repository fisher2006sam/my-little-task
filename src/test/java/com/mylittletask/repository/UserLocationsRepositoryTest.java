package com.mylittletask.repository;

import com.mylittletask.document.UserLocationDocument;
import org.junit.jupiter.api.Test;
import org.reactivestreams.Publisher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.mongo.DataMongoTest;
import reactor.test.StepVerifier;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

/**
 * This is a test for checking emulated database functionality:
 * - Emulating saving/getting user locations into/from emulated database
 *
 * @author Erchenko Igor
 */
@DataMongoTest
class UserLocationsRepositoryTest {

    @Autowired
    private UserLocationsRepository repository;

    @Test
    void findAllByUserId() {
        UserLocationDocument locationDocument = new UserLocationDocument();
        locationDocument.setUserId("2e3b11b0-07a4-4873-8de5-d2ae2eab26b2");
        locationDocument.setCreatedOn("2022-02-08T11:44:00.524");
        locationDocument.setLatitude(52.25742342295784);
        locationDocument.setLongitude(10.540583401747602);

        Publisher<UserLocationDocument> publisher = repository.deleteAll().thenMany(repository.save(locationDocument));
        StepVerifier
                .create(publisher)
                .consumeNextWith(location -> {
                    assertNotNull(location);
                    assertEquals(location.getUserId(), locationDocument.getUserId());
                    assertEquals(location.getCreatedOn(), locationDocument.getCreatedOn());
                    assertEquals(location.getLatitude(), locationDocument.getLatitude());
                    assertEquals(location.getLongitude(), locationDocument.getLongitude());
                })
                .verifyComplete();
    }
}