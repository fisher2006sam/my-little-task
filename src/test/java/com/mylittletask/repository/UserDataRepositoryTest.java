package com.mylittletask.repository;

import com.mylittletask.document.UserDataDocument;
import org.junit.jupiter.api.Test;
import org.reactivestreams.Publisher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.mongo.DataMongoTest;
import reactor.test.StepVerifier;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

/**
 * This is a test for checking emulated database functionality:
 * - Emulating saving/getting userdata into/from emulated database
 *
 * @author Erchenko Igor
 */
@DataMongoTest
class UserDataRepositoryTest {

    @Autowired
    private UserDataRepository repository;

    @Test
    void findUserDataDocumentByUserId() {
        UserDataDocument userDataDocument = UserDataDocument.builder()
                .userId("2e3b11b0-07a4-4873-8de5-d2ae2eab26b2")
                .email("alex.schmid@gmail.com")
                .firstName("Alex")
                .secondName("Schmid")
                .build();

        Publisher<UserDataDocument> publisher = repository.deleteAll().thenMany(repository.save(userDataDocument));
        StepVerifier
                .create(publisher)
                .consumeNextWith(data -> {
                    assertNotNull(data);
                    assertEquals(data.getUserId(), userDataDocument.getUserId());
                    assertEquals(data.getEmail(), userDataDocument.getEmail());
                    assertEquals(data.getFirstName(), userDataDocument.getFirstName());
                    assertEquals(data.getSecondName(), userDataDocument.getSecondName());
                })
                .verifyComplete();
    }
}